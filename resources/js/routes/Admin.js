const Admin = [
    // START ADMIN //
    {
        name: 'admin',
        path: '/admin',
        component: () => import(/* webpackChunkName: "admin" */ '../pages/admin/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['Admin',  'Dashboard']
        }
    },
    {
        name: 'admin.setting',
        path: '/admin/setting',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/setting/Index'),
        meta: {
            title: `Setting`,
            auth: true,
            breadcrumb: ['Admin', 'Setting']
        }
    },
    {
        name: 'admin.buku',
        path: '/admin/buku',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/buku/Index'),
        meta: {
            title: `buku`,
            auth: true,
            breadcrumb: ['Admin', 'buku']
        }
    },
           { name: 'admin.aktivitas',
        path: '/admin/aktivitas',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/aktivitas/Index'),
        meta: {
            title: `aktivitas`,
            auth: true,
            breadcrumb: ['Admin', 'aktivitas']
        }
    },
    {
        name: 'admin.siswa',
        path: '/admin/siswa',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/siswa/Index'),
        meta: {
            title: `siswa`,
            auth: true,
            breadcrumb: ['Admin', 'siswa']
        }
    },
    {
        name: 'admin.tag',
        path: '/admin/tag',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/tag/Index'),
        meta: {
            title: `tag`,
            auth: true,
            breadcrumb: ['Admin', 'tag']
        }
    },
    {
        name: 'admin.pinjam',
        path: '/admin/pinjam',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/pinjam/Index'),
        meta: {
            title: `pinjam`,
            auth: true,
            breadcrumb: ['Admin', 'pinjam']
        }
    },
    {
        name: 'admin.kembali',
        path: '/admin/kembali',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/kembali/Index'),
        meta: {
            title: `kembali`,
            auth: true,
            breadcrumb: ['Admin', 'kembali']
        }
    },
    {
        name: 'admin.daftar_pinjam',
        path: '/admin/daftar_pinjam',
        component: () => import(/* webpackChunkName: "admin.setting" */ '../pages/admin/daftar_pinjam/Index'),
        meta: {
            title: `daftar_pinjam`,
            auth: true,
            breadcrumb: ['Admin', 'daftar_pinjam']
        }
    },
    // END ADMIN //
];

export default Admin