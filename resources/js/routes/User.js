const User = [
    // START USER //
    {
        name: 'user',
        path: '/user',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/home/Index'),
        meta: {
            title: `Dashboard`,
            auth: true,
            breadcrumb: ['User',  'Dashboard']
        }
    },
        {
        name: 'user.detail',
        path: '/user/:uuid/detail',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/detail/Index'),
        meta: {
            title: `Detail`,
            auth: true,
            breadcrumb: ['detail',  'Dashboard']
        }
    },
        {
        name: 'user.aktivitas',
        path: '/user/aktivitas',
        component: () => import(/* webpackChunkName: "user" */ '../pages/user/aktivitas/Index'),
        meta: {
            title: `Aktivitas`,
            auth: true,
            breadcrumb: ['User',  'Aktivitas']
        }
    },

    // END USER //
];

export default User