import objectPath from "object-path";
import config from "../config/layout.config.json";

const HtmlClass = {
  config: config,

  addBodyClass(className) {
      document.body.classList.add(className);
  },
  removeBodyClass(className) {
      document.body.classList.remove(className);
  },

  getConfig(obj) {
    return objectPath.get(this.config, obj);
  },

  init() {
    // init base layout
    this.initLayout();

    // init header and subheader menu
    this.initHeader();
    this.initSubheader();

    // init aside and aside menu
    this.initAside();

    // init footer
    this.initFooter();

    // init themes
    this.initThemes();
  },

  /**
   * Init Layout
   */
  initLayout() {
    var vm = this;
    if (objectPath.has(this.config, "self.body.class")) {
      const _selfBodyClass = objectPath
        .get(this.config, "self.body.class")
        .toString();
      if (_selfBodyClass) {
        const bodyClasses = _selfBodyClass.split(" ");
        bodyClasses.forEach(cssClass => {
          vm.addBodyClass(cssClass);
        });
      }
    }

    const bgImage = objectPath.get(this.config, "self.body.background-image");
    if (typeof bgImage !== "undefined") {
      document.body.style.backgroundImage = `url(${bgImage})`;
    }

    // Offcanvas directions
    vm.addBodyClass("quick-panel-right");
    vm.addBodyClass("demo-panel-right");
    vm.addBodyClass("offcanvas-right");

    // Properly close mobile header menu
    vm.removeBodyClass("header-menu-wrapper-on");
  },

  /**
   * Init Header
   */
  initHeader() {
    var vm = this;
    // Fixed header
    if (objectPath.get(this.config, "header.self.fixed.desktop")) {
      vm.addBodyClass("header-fixed");
      // store.dispatch(ADD_CLASSNAME, {
        // position: "header",
        // className: "header-fixed"
      // });
    } else {
      vm.addBodyClass("header-static");
    }

    if (objectPath.get(this.config, "header.self.fixed.mobile")) {
      vm.addBodyClass("header-mobile-fixed");
      // store.dispatch(ADD_CLASSNAME, {
        // position: "header_mobile",
        // className: "header-mobile-fixed"
      // });
    }

    if (objectPath.get(this.config, "header.menu.self.display")) {
      // store.dispatch(ADD_CLASSNAME, {
        // position: "header_menu",
        // className: `header-menu-layout-${objectPath.get(
          // this.config,
          // "header.menu.self.layout"
        // )}`
      // });

      // Menu
      if (objectPath.get(this.config, "header.menu.self.root-arrow")) {
        // store.dispatch(ADD_CLASSNAME, {
          // position: "header_menu",
          // className: "header-menu-root-arrow"
        // });
      }
    }
  },

  /**
   * Init Subheader
   */
  initSubheader() {
    var vm = this;
    // Fixed content head
    if (
      objectPath.get(this.config, "subheader.fixed") &&
      objectPath.get(this.config, "header.self.fixed.desktop")
    ) {
      vm.addBodyClass("subheader-fixed");
    }

    if (objectPath.get(this.config, "subheader.display")) {
      vm.addBodyClass("subheader-enabled");
    }

    if (objectPath.has(this.config, "subheader.style")) {
      // store.dispatch(
        // ADD_BODY_CLASSNAME,
        // `subheader-${objectPath.get(this.config, "subheader.style")}`
      // );
    }
  },

  /**
   * Init Aside
   */
  initAside() {
    var vm = this;
    // Reset aside class in body
    vm.removeBodyClass("aside-enabled");
    vm.removeBodyClass("aside-fixed");
    vm.removeBodyClass("aside-static");
    vm.removeBodyClass("aside-minimize");

    if (objectPath.get(this.config, "aside.self.display") !== true) {
      return;
    }

    // Add aside class enabled in body
    vm.addBodyClass("aside-enabled");

    // Fixed Aside
    if (objectPath.get(this.config, "aside.self.fixed")) {
      vm.addBodyClass("aside-fixed");
      // store.dispatch(ADD_CLASSNAME, {
        // position: "aside",
        // className: "aside-fixed"
      // });
    } else {
      vm.addBodyClass("aside-static");
    }

    // Default fixed
    if (objectPath.get(this.config, "aside.self.minimize.default")) {
      vm.addBodyClass("aside-minimize");
    }

    // Dropdown Submenu
    if (objectPath.get(this.config, "aside.menu.dropdown")) {
      // store.dispatch(ADD_CLASSNAME, {
        // position: "aside_menu",
        // className: "aside-menu-dropdown"
      // });
    }
  },

  /**
   * Init Footer
   */
  initFooter() {
    var vm = this;
    // Fixed header
    if (objectPath.get(this.config, "footer.fixed")) {
      vm.addBodyClass("footer-fixed");
    }
  },

  /**
   * Import theme SCSS file based on the selected theme
   */
  initThemes() {
    var vm = this;
    if (objectPath.get(this.config, "header.self.theme")) {
      const theme = objectPath.get(this.config, "header.self.theme");
      import(`../src/sass/themes/layout/header/base/${theme}.scss`);
    }

    if (objectPath.get(this.config, "header.menu.desktop.submenu.theme")) {
      const theme = objectPath.get(
        this.config,
        "header.menu.desktop.submenu.theme"
      );
      import(`../src/sass/themes/layout/header/menu/${theme}.scss`);
    }

    if (objectPath.get(this.config, "brand.self.theme")) {
      const theme = objectPath.get(this.config, "brand.self.theme");
      import(`../src/sass/themes/layout/brand/${theme}.scss`);
    }

    if (objectPath.get(this.config, "aside.self.theme")) {
      const theme = objectPath.get(this.config, "aside.self.theme");
      import(`../src/sass/themes/layout/aside/${theme}.scss`);
    }
  }
};

export default HtmlClass;
