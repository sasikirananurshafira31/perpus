String.prototype.ucfirst = function() {
    return `${this[0].toUpperCase()}${this.slice(1)}`
}

String.prototype.strip_tags = function() {
    var str = this.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}

String.prototype.ucword = function() {
    var str = this;
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    return str;
}

String.prototype.indo = function() {

    var months = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    var date = this.split('-');
    var day = date[2];
    var month = parseInt(date[1]);
    var year = date[0];
    return day + ' ' + months[month] + ' ' + year;
}

String.prototype.tgl_indo = function(time=false) {

    var months = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    var d = this.split(' ');
    var date = d[0].split('-');
    var day = date[2];
    var month = parseInt(date[1]);
    var year = date[0];
    if(time){
        return day + ' ' + months[month] + ' ' + year + ' '+d[1];
    }else{
        return day + ' ' + months[month] + ' ' + year;
    }
}

Number.prototype.rupiah = function() {
    var str = this.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    return str;
}

String.prototype.rupiah = function() {
    var str = this.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    return str;
}

window.KTApp={block:function(e,s){var n,o=$(e),t='<span class="spinner '+((s=$.extend(!0,{opacity:.2,overlayColor:"#000000",type:"v2",size:"",state:"primary",centerX:!0,centerY:!0,message:"Proccessing...",shadow:!0,width:"auto"},s)).type?"spinner-"+s.type:"")+" "+(s.state?"spinner-"+s.state:"")+" "+(s.size?"spinner-"+s.size:"")+'"></span';if(s.message&&s.message.length>0){var a="blockui "+(!1===s.shadow?"blockui":"");n='<div class="'+a+'"><span>'+s.message+"</span>"+t+"</div>",o=document.createElement("div"),$("body").prepend(o),KTUtil.addClass(o,a),o.innerHTML=n,s.width=KTUtil.actualWidth(o)+10,KTUtil.remove(o),"body"==e&&(n='<div class="'+a+'" style="margin-left:-'+s.width/2+'px;"><span>'+s.message+"</span><span>"+t+"</span></div>")}else n=t;var i={message:n,centerY:s.centerY,centerX:s.centerX,css:{top:"30%",left:"50%",border:"0",padding:"0",backgroundColor:"none",width:s.width},overlayCSS:{backgroundColor:s.overlayColor,opacity:s.opacity,cursor:"wait",zIndex:"body"==e?1100:10},onUnblock:function(){o&&o[0]&&(KTUtil.css(o[0],"position",""),KTUtil.css(o[0],"zoom",""))}};"body"==e?(i.css.top="50%",$.blockUI(i)):(o=$(e)).block(i)},unblock:function(e){e&&"body"!=e?$(e).unblock():$.unblockUI()}};
