// Keenthemes plugins
import KTUtil from "../src/components/util.js";
window.KTUtil = KTUtil;

import KTCard from "../src/components/card.js";
window.KTCard = KTCard;

import KTCookie from "../src/components/cookie.js";
window.KTCookie = KTCookie;

import KTDialog from "../src/components/dialog.js";
window.KTDialog = KTDialog;

import KTHeader from "../src/components/header.js";
window.KTHeader = KTHeader;

import KTImageInput from "../src/components/image-input.js";
window.KTImageInput = KTImageInput;

import KTMenu from "../src/components/menu.js";
window.KTMenu = KTMenu;

import KTOffcanvas from "../src/components/offcanvas.js";
window.KTOffcanvas = KTOffcanvas;

import KTScrolltop from "../src/components/scrolltop.js";
window.KTScrolltop = KTScrolltop;

import KTToggle from "../src/components/toggle.js";
window.KTToggle = KTToggle;

import KTWizard from "../src/components/wizard.js";
window.KTWizard = KTWizard;

// Metronic layout base js
import KTLayoutAside from "../src/layout/base/aside.js";
window.KTLayoutAside = KTLayoutAside;

import KTLayoutAsideMenu from "../src/layout/base/aside-menu.js";
window.KTLayoutAsideMenu = KTLayoutAsideMenu;

import KTLayoutAsideToggle from "../src/layout/base/aside-toggle.js";
window.KTLayoutAsideToggle = KTLayoutAsideToggle;

import KTLayoutBrand from "../src/layout/base/brand.js";
window.KTLayoutBrand = KTLayoutBrand;

import KTLayoutContent from "../src/layout/base/content.js";
window.KTLayoutContent = KTLayoutContent;

import KTLayoutFooter from "../src/layout/base/footer.js";
window.KTLayoutFooter = KTLayoutFooter;

import KTLayoutHeader from "../src/layout/base/header.js";
window.KTLayoutHeader = KTLayoutHeader;

import KTLayoutHeaderMenu from "../src/layout/base/header-menu.js";
window.KTLayoutHeaderMenu = KTLayoutHeaderMenu;

import KTLayoutHeaderTopbar from "../src/layout/base/header-topbar.js";
window.KTLayoutHeaderTopbar = KTLayoutHeaderTopbar;

import KTLayoutStickyCard from "../src/layout/base/sticky-card.js";
window.KTLayoutStickyCard = KTLayoutStickyCard;

import KTLayoutStretchedCard from "../src/layout/base/stretched-card.js";
window.KTLayoutStretchedCard = KTLayoutStretchedCard;

import KTLayoutSubheader from "../src/layout/base/subheader.js";
window.KTLayoutSubheader = KTLayoutSubheader;

// Metronic layout extended js
import KTLayoutChat from "../src/layout/extended/chat.js";
window.KTLayoutChat = KTLayoutChat;

import KTLayoutExamples from "../src/layout/extended/examples.js";
window.KTLayoutExamples = KTLayoutExamples;

import KTLayoutQuickActions from "../src/layout/extended/quick-actions.js";
window.KTLayoutQuickActions = KTLayoutQuickActions;

import KTLayoutQuickCartPanel from "../src/layout/extended/quick-cart.js";
window.KTLayoutQuickCartPanel = KTLayoutQuickCartPanel;

import KTLayoutQuickNotifications from "../src/layout/extended/quick-notifications.js";
window.KTLayoutQuickNotifications = KTLayoutQuickNotifications;

import KTLayoutQuickPanel from "../src/layout/extended/quick-panel.js";
window.KTLayoutQuickPanel = KTLayoutQuickPanel;

import KTLayoutQuickSearch from "../src/layout/extended/quick-search.js";
window.KTLayoutQuickSearch = KTLayoutQuickSearch;

import KTLayoutQuickUser from "../src/layout/extended/quick-user.js";
window.KTLayoutQuickUser = KTLayoutQuickUser;

import KTLayoutScrolltop from "../src/layout/extended/scrolltop.js";
window.KTLayoutScrolltop = KTLayoutScrolltop;

import KTLayoutSearch from "../src/layout/extended/search.js";
window.KTLayoutSearch = KTLayoutSearch;
