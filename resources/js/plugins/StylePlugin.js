const StylePlugin = {
    install(Vue) {
        Vue.mixin({
            mounted() {
                const css = this.$options.style;
                if (!css) return;
                this.$styleTag = document.createElement('style');
                this.$styleTag.appendChild(document.createTextNode(css));
                $("link[name=gfont]").after(this.$styleTag)
                // document.head.appendChild();
            },
            destroyed() {
                if (this.$styleTag) {
                    this.$styleTag.remove();
                }
            }
        });
    }
};

export default StylePlugin