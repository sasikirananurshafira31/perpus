require('./bootstrap');

window.Vue = require('vue');

import _ from 'lodash';
import router from './router';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import auth from './auth';
import VueAuth from '@websanova/vue-auth';
import App from './pages/App';
import Paginate from './components/Paginate';
import StylePlugin from './plugins/StylePlugin';
import './helpers/AppHelper';
import Swal from 'sweetalert2';




Vue.router = router;
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueAuth, auth);
Vue.use(StylePlugin);

import "popper.js";
import "tooltip.js";
import PerfectScrollbar from "perfect-scrollbar";
window.PerfectScrollbar = PerfectScrollbar;

window.Swal = Swal;

import vuetify from "./plugins/vuetify";
import "./plugins/portal-vue";
import "./plugins/bootstrap-vue";
import "./plugins/perfect-scrollbar";
// import "./plugins/highlight-js";
import "./plugins/inline-svg";
// import "./plugins/apexcharts";
import "./plugins/treeselect";
import "./plugins/metronic";
import "@mdi/font/css/materialdesignicons.css";
import "./plugins/formvalidation";

import toastr from "toastr";
window.toastr = toastr;

Vue.component('mti-paginate', Paginate)

axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api/v1`

Vue.config.productionTip = false;

Vue.mixin({
  	data: function() {
    	return {
      		get api_url() {
        		return `${process.env.MIX_APP_URL}/api/v1`;
      		},
      		get base_url() {
        		return `${process.env.MIX_APP_URL}`;
      		},
      		get app_name() {
        		return `${process.env.MIX_APP_NAME}`;
      		}
    	}
  	}
});

axios.interceptors.response.use(null, function(error) {
    if (error.response.status === 503) {
        var title = error.response.data.message.title;
        var html = error.response.data.message.description;
        return swal.fire({
          	title: '<strong>'+title+'</strong>',
          	icon: 'error',
          	html: html,
        }).then(function() {document.location.reload()})
    }
  	return Promise.reject(error);
});

const app = new Vue({
    render: h => h(App),
    router: router,
    vuetify
}).$mount('#root');
