<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<base href="/">
		<meta charset="utf-8" />
		<title>{{ $config->name }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="{{ str_replace('&nbsp;', ' ', strip_tags($config->description)) }}">
		<meta name="keywords" content="{{ $config->name }}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="title" content="{{ $config->name }}">
		<link name="gfont" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link rel="shortcut icon" href="{{ asset('assets/media/logos/favicon.png') }}" />
		<meta name="twitter:card" content="summary">
		<meta name="twitter:description" content="{{ str_replace('&nbsp;', ' ', strip_tags($config->description)) }}">
		<meta name="twitter:title" content="{{ $config->name }}">
		<meta name="twitter:image" content="{{ asset('assets/media/logos/favicon.png') }}">
	</head>
	<body>
		<div id="root"></div>
		<script src="{{ asset('js/app.bundle.js?v=7.1.8') }}"></script>
		<script src="{{ asset('js/jQuery.blockui.min.js') }}"></script>
	</body>
</html>