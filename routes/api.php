<?php

use Illuminate\Http\Request;

Route::prefix('v1')->group(function() {

	Route::get('faq', 'FaqController@show');
	Route::get('setting', 'SettingController@show');
	
	Route::prefix('auth')->group(function(){
		Route::get('verify/{verification_code}', 'AuthController@verifyUser');
		Route::post('register', 'AuthController@register');
		Route::post('login', 'AuthController@login');
		Route::post('recover', 'AuthController@recover');
		Route::post('recover/check', 'AuthController@checkRecover');
		Route::post('recover/send', 'AuthController@postRecover');
	});

				Route::prefix('homenoauth')->group(function () {
				Route::get('{uuid}/getproduk', 'Homecontroller@getproduk');
				Route::get('getdatacatalog', 'Homecontroller@getdatatags');
				Route::post('pinjam', 'Homecontroller@pinjam');
				Route::post('index', 'Homecontroller@index');
			});



	Route::group(['middleware' => ['jwt.auth']], function() {

		Route::prefix('auth')->group(function(){
		    Route::post('logout', 'AuthController@logout');
		    Route::get('user', 'AuthController@user');
		    Route::get('refresh', 'AuthController@refresh');
		});
	    Route::post('test', 'AuthController@testing');

	    Route::prefix('user')->group(function(){
			Route::prefix('home')->group(function () {
				Route::get('{uuid}/getproduk', 'Homecontroller@getproduk');
				Route::get('getdatacatalog', 'Homecontroller@getdatatags');
				Route::post('pinjam', 'Homecontroller@pinjam');
				Route::post('index', 'Homecontroller@index');
			});

			Route::prefix('aktivitas')->group(function () {
				Route::post('index/{id}', 'AktivitasController@index');
				Route::get('{uuid}/getdetail', 'AktivitasController@getdetail');
				Route::get('{uuid}/batal', 'AktivitasController@batal');
				Route::get('{uuid}/selesai', 'TransaksiController@selesai');
			});
		});

	    Route::group(['middleware' => ['admin.access']], function() {

	    	Route::prefix('aktivitas')->group(function () {
					Route::post('index/{id}', 'AdminAktivitasController@index');
					Route::get('{uuid}/getdetail', 'AdminAktivitasController@getdetail');
					Route::get('{uuid}/kirim', 'AdminAktivitasController@kirim');
					Route::get('{uuid}/selesai', 'AdminAktivitasController@selesai');
				});

			Route::prefix('buku')->group(function(){
				Route::post('index', 'BukuController@index');
				Route::get('code', 'BukuController@code');
				Route::get('getselect', 'BukuController@getselect');
				Route::post('create', 'BukuController@store');
				Route::delete('{uuid}/delete', 'BukuController@destroy');
				Route::get('{uuid}/edit', 'BukuController@show');
				Route::post('{uuid}/update', 'BukuController@update');
			});
			Route::prefix('tag')->group(function(){
				Route::post('index', 'TagController@index');
				Route::post('create', 'TagController@store');
				Route::delete('{uuid}/delete', 'TagController@destroy');
				Route::get('{uuid}/edit', 'TagController@show');
				Route::post('{uuid}/update', 'TagController@update');
			});
			Route::prefix('daftar_pinjam')->group(function(){
				Route::post('index', 'DaftarPinjamController@index');
				Route::delete('{uuid}/delete', 'DaftarPinjamController@destroy');
			});
			Route::prefix('siswa')->group(function(){
				Route::post('index', 'SiswaController@index');
				Route::post('create', 'SiswaController@store');
				Route::delete('{uuid}/delete', 'SiswaController@destroy');
				Route::get('{uuid}/edit', 'SiswaController@show');
				Route::post('{uuid}/update', 'SiswaController@update');
			});
			Route::prefix('pinjam')->group(function(){
				Route::get('{nisn}/getsiswa', 'PinjamController@getsiswa');
				Route::post('create', 'PinjamController@store');
				Route::get('{kode}/getbuku', 'PinjamController@getbuku');
			});
			Route::prefix('kembali')->group(function(){
				Route::get('{nisn}/getsiswa', 'KembaliController@getsiswa');
				Route::post('create', 'KembaliController@store');
				Route::get('{kode}/getbuku', 'KembaliController@getbuku');
			});

			Route::post('setting/update', 'SettingController@update');
		});
	});
});
