<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Buku;
use App\Models\Aktivitas;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    public function getdatatags(Request $request)
    {        
        $tags = Tag::get();

        return response()->json([
            'tags' => $tags,
        ]);

    }

    public function pinjam(Request $request)
    {
        $check = Aktivitas::where([['user_id', '=', Auth::user()->id],['buku_id', '=', $request->id], ['status', '=', '1'], ['status', '=', '2']])->get();

        $jumlah = Aktivitas::where([['user_id', '=', Auth::user()->id], ['status', '=', '0']])->orWhere([['user_id', '=', Auth::user()->id], ['status', '=', '1']])->orWhere([['user_id', '=', Auth::user()->id], ['status', '=', '2']])->get();
        if(count($jumlah) >= 3){
            return response()->json(['pesan' => 'buku dipesan sudah lebih dari 3'], 400);
        }

        $date = date('Y-m-d');
        $date1 = str_replace('-', '/', $date);
        $tomorrow = date('Y-m-d',strtotime($date1 . "+3 days"));

        if(count($check) != 0){
            return response()->json(['pesan' => 'buku sudah dipesan'], 400);
        } else {
            $data = Aktivitas::create([
                'buku_id' =>  $request->id,
                'user_id' => Auth::user()->id,
                'tgl_pinjam' => date('Y-m-d'),
                'tgl_pengembalian' => $tomorrow
            ]);

            if($data){
                $oi = Buku::where('id', '=', $request->id)->first()->update(['status' => '1']);
                return response()->json(['pesan' => $oi]);
            } else {
                return response()->json(['pesan' => 'gagal'], 400);
            }
        }
    }

    public function getproduk($uuid)
    {
        $data = Buku::with(['tag', 'detail_buku'])->where('uuid', '=', $uuid)->first();

        if($data == null){
            return response()->json([
                'pesan' => 'tidak ada data'
            ], 400);
        }
        return response()->json([
            'data' => $data
        ]);
    }

    public function index(Request $request)
    {
        if($request->tag_id == 'all'){
            $request->tag_id = '';
        }

        if($request->tag_id == ''){
            $user = Buku::where([['status', '=', '0'],[function($q) use ($request) {
                                        $q->where('judul_buku', 'LIKE', '%'.$request->search.'%')
                                        ->orWhere('kode', 'LIKE', '%'.$request->search.'%');
                                    }]])->orderBy('id','asc')->get();
        } else {
            $user = Buku::where([['status', '=', '0'],['tags_id', '=', $request->tag_id],[(function($q) use ($request) {
                $q->where('judul_buku', 'LIKE', '%'.$request->search.'%')
                ->orWhere('kode', 'LIKE', '%'.$request->search.'%');
            })]])->orderBy('id','asc')->get();

        }
        return response()->json([
            'produks' => $user,
        ]);
    }
}