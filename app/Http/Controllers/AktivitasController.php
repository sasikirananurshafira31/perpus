<?php
//user
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aktivitas;
use App\Models\Buku;

use Auth;


class AktivitasController extends Controller
{
    public function index(Request $request, $status)
    {
        $id = Auth::user()->id;
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Aktivitas::with('buku')->where([['user_id', '=', $id],['status', '=', $status],[function($q) use ($request) {
                $q->where('tgl_pinjam', 'LIKE', '%'.$request->search.'%');
            }]])->orderBy('id','asc')->paginate($per);

            // Add Columns
            switch($status){
                case 1:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 2:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 3:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 0:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-danger btn-icon-md batal" title="Batalkan Pemesanan" data-id="'.$a->uuid.'"><i class="la la-trash kt-font-info"></i></span><span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                

            }
            return response()->json($user);

        }else{
            abort(404);
        }
    }
    public function getdetail($uuid)
    {
        $data = Aktivitas::with(['buku', 'user'])->where('uuid', '=', $uuid)->first();
        switch($data->status) {
            case 0: $data->status = '<span class="label label-info label-inline mr-2">Belum diambil</span>';
            break;
            case 1: $data->status = '<span class="label label-warning label-inline mr-2">Sedang Di pinjam</span>';
            break;
            case 2: $data->status = '<span class="label label-success label-inline mr-2">Selesai</span>';
            break;
            case 3: $data->status = '<span class="label label-danger label-inline mr-2">Dibatalkan</span>';
            break;
        }
        return response()->json([
            'data' => $data
        ]);
    }
    public function batal($uuid)
    {
        $data = Aktivitas::where('uuid', '=', $uuid)->first();
        $data->update([
            'status' => '3'
        ]);

        $oi = Buku::where('id', '=', $data->buku_id)->first()->update([
            'status' => '0']);


        if(!$data){
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        } else {
            return response()->json([
                'pesan' => 'sukses'
            ]);

        }

    }
}
