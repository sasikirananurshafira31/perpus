<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use Validator;

class TagController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Tag::where(function($q) use ($request) {
                $q->where('nama', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->paginate($per);

            // Add Columns
            $user->map(function($a) {
            	if ($a->status == "0") {
            		$a->status = "Tersedia";
            	} else {
            		$a->status = "Telah Dipinjam    ";
            	}
                $a->action = '<button class="btn btn-sm btn-clean edit" title="Edit" data-id="'.$a->uuid.'"><i class="las la-pencil-alt p-warna text-warning"></i></button> <button class="btn btn-sm btn-clean delete" title="Hapus" data-id="'.$a->uuid.'"><i class="las la-trash p-warnaa text-danger"></i></button>';

                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }

    public function store(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $validator = Validator::make($request->all(), [
                'nama'  => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ], 400);
            }

            $data = Tag::create($request->all());

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menambah data.'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menambah data.'
                ], 500);
            }

        }else{
            abort(404);
        }     
    }

    public function show($uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Tag::findByUuid($uuid);

            return response()->json([
                'status'    => true,
                'data'      => $data
            ],200);
        }else{
            abort(404);
        }     
    }

    public function update(Request $request, $uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Tag::findByUuid($uuid);

            $validator = Validator::make($request->all(), [
                'nama'  => 'required|string',

            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => $validator->messages()->first()
                ], 400);
            }

            $data->update($request->all());

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil merubah data.'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal merubah data.'
                ], 500);
            }

        }else{
            abort(404);
        }   
    }
    
    public function destroy($uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Tag::findByUuid($uuid);

            if (!$data) {
                return response()->json([
                    'status' => false,
                    'message' => 'data tidak ada'
                ], 400);
            }

            $data->delete();

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menghapus data '
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menghapus data.'
                ], 500);
            }

        }else{
            abort(404);   
        }
    }

}
