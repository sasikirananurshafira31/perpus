<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\Aktivitas;
use App\Models\Buku;


use Illuminate\Http\Request;

class AdminAktivitasController extends Controller
{
    public function index(Request $request, $status)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Aktivitas::with(['buku', 'user'])->where('status', '=', $status)->whereHas('buku', function($q) use ($request){
                $q->where('kode', 'LIKE', '%'.$request->search.'%');

            })->orderBy('id','asc')->paginate($per);

            // Add Columns
            switch($status){
                case 1:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon text-warning btn-icon-md selesai" title="Selesai" data-id="'.$a->uuid.'"><i class="las la-upload text-success"></i></span><span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 2:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 3:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                
                case 0:
                    $user->map(function($a) {
                        $a->action = '<span class="btn btn-sm btn-clean btn-icon text-warning btn-icon-md kirim" title="Kirim Pemesanan" data-id="'.$a->uuid.'"><i class="la la-shipping-fast text-warning"></i></span><span class="btn btn-sm btn-clean btn-icon btn-outline-pirmary btn-icon-md detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                        return $a;
                    });
                    break;
                

            }
            return response()->json($user);

        }else{
            abort(404);
        }
    }
    public function getdetail($uuid)
    {
        $data = Aktivitas::with(['buku', 'user'])->where('uuid', '=', $uuid)->first();
        switch($data->status) {
            case 0: $data->status = '<span class="label label-info label-inline mr-2">Belum diambil</span>';
            break;
            case 1: $data->status = '<span class="label label-warning label-inline mr-2">Sedang Di pinjam</span>';
            break;
            case 2: $data->status = '<span class="label label-success label-inline mr-2">Selesai</span>';
            break;
            case 3: $data->status = '<span class="label label-danger label-inline mr-2">Dibatalkan</span>';
            break;
        }
        return response()->json([
            'data' => $data
        ]);
    }
    public function kirim($uuid)
    {
        $data = Aktivitas::findByUuid($uuid);
        $data->update([
            'status' => '1'
        ]);

        if(!$data){
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        } else {
            return response()->json([
                'pesan' => 'sukses'
            ]);

        }
    }
    public function selesai($uuid)
    {
        $data = Aktivitas::findByUuid($uuid);

        $denda = 1000; // denda 1000 / hari


        $tgl1 = strtotime($data->tgl_pengembalian); 
        $tgl2 = strtotime(date('Y-m-d')); 

        $jarak = $tgl2 - $tgl1;

        $hari = $jarak / 60 / 60 / 24;

        $denda = $denda * $hari;

        if($denda < 0){
            $denda = 0;
        }



        $data->update([
            'status' => '2',
            'denda' => $denda
        ]);


        if(!$data){
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        } else {
            Buku::where('id', '=', $data->buku_id)->first()->update([
                'status' => '0'
            ]);
            return response()->json([
                'pesan' => 'sukses'
            ]);

        }
    }
    public function batal($uuid)
    {
        $data = Aktivitas::where('uuid', '=', $uuid)->first();
        $data->update([
            'status' => '3'
        ]);



        if(!$data){
            return response()->json([
                'pesan' => 'gagal'
            ], 500);
        } else {
                    $oi = Buku::where('id', '=', $data->buku_id)->first()->update([
            'status' => '0']);
            return response()->json([
                'pesan' => 'sukses'
            ]);

        }

    }
}
