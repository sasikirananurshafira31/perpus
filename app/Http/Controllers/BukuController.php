<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;
use App\Models\BukuDetail;
use App\Models\Tag;
use Validator;
use Milon\Barcode\DNS2D;
use Str,File;

class BukuController extends Controller
{
         public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Buku::where(function($q) use ($request) {
                $q->where('judul_buku', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('kode', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->paginate($per);

            // Add Columns
            $user->map(function($a) {
            	if ($a->status == "0") {
            		$a->status = "Tersedia";
            	} else {
            		$a->status = "Telah Dipinjam";
            	}
                $a->qrcode = '<img src="'.$a->foto_dir.'" alt="barcode" height="100" width="100"  />';
                $a->action = '<button class="btn btn-sm btn-clean edit" title="Edit" data-id="'.$a->uuid.'"><i class="las la-pencil-alt p-warna text-warning"></i></button> <button class="btn btn-sm btn-clean delete" title="Hapus" data-id="'.$a->uuid.'"><i class="las la-trash p-warnaa text-danger"></i></button>';

                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }

    public function getselect()
    {
        $data = Tag::get();

        return response()->json([
            'tag' => $data
        ]);
    }

    public function code()
    {
        $a = Buku::get('kode');
        $y = array();
        $z = array();
        $c = 0;
        $count = count($a);

        if(!isset($a)){
            return 1;
        }else{
            foreach($a as $value){
                $k = explode('"', $value);
                array_push($y, $k[3]);
            }
            foreach($y as $value){
                $k = explode('-', $value);
                array_push($z, $k[1]);
            }


            sort($z);
                foreach($z as $value){
                    $c++;

                    if(!($c == $value)){
                        return $c;
                        break;
                    }
                }
            return $count+1;
        }
    }

    public function store(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {


            $validator = Validator::make($request->all(), [
                'kode'  => 'required|string',
                'judul_buku'  => 'required|string',
                'tags_id'  => 'required',
                'foto'  => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ], 400);
            }
    
    
            $exploded = explode(',' ,$request['foto']);
            $decoded = base64_decode($exploded[1]);
    
            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
    
            else
                $extension = 'png';
    
            $FileName = Str::random(30).'.'.$extension;
            $path = public_path().'/images/foto_buku/'.$FileName;
    
            file_put_contents($path, $decoded);
            $request['foto'] = $FileName;
            $data = Buku::create([
                'foto' => $request['foto'],
                'judul_buku' => $request->judul_buku,
                'kode' => $request->kode,
                'tags_id' => $request->tags_id
            ]);

            BukuDetail::create([
                'buku_id' => $data->id,
                'penerbit' => $request->penerbit,
                'thn_terbit' => $request->thn_terbit,
                'pengarang' => $request->pengarang,
            ]);

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menambah data.'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menambah data.'
                ], 500);
            }

        }else{
            abort(404);
        }     
    }

    public function show($uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Buku::findByUuid($uuid, 'detail_buku');

                $data->penerbit = $data->detail_buku->penerbit;
                $data->thn_terbit = $data->detail_buku->thn_terbit;
                $data->pengarang = $data->detail_buku->pengarang;

            return response()->json([
                'status'    => true,
                'data'      => $data
            ],200);
        }else{
            abort(404);
        }     
    }

    public function update(Request $request, $uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Buku::findByUuid($uuid);

            $validator = Validator::make($request->all(), [
                'kode'  => 'required|string',
                'judul_buku'  => 'required|string',
                'tags_id'  => 'required',
                'foto' => 'required'

            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => $validator->messages()->first()
                ], 400);
            }

             if($request->foto != $request->foto_dir){
            if(file_exists(public_path().'/images/foto_buku/'.$data['foto'])){
                File::delete('images/foto_buku/'.$data['foto']);
                // return 'ada';
            }
    
            // return 'tidak ada';
    
            $exploded = explode(',' ,$request->foto);
            $decoded = base64_decode($exploded[1]);
    
            if(str_contains($exploded[0], 'jpeg'))
                $extension = 'jpg';
    
            else
                $extension = 'png';
    
            $FileName = Str::random(30).'.'.$extension;
            $path = public_path().'/images/foto_buku/'.$FileName;
    
            file_put_contents($path, $decoded);
            $request->foto = $FileName;
        } else {
            $request->foto = $data['foto'];
        }


            $data->update([
                'judul_buku' => $request->judul_buku,
                'kode' => $request->kode,
                'tags_id' => $request->tags_id,
                'foto' => $request->foto
            ]);

            BukuDetail::where('buku_id', $data->id)->first()->update([
                'penerbit' => $request->penerbit,
                'thn_terbit' => $request->thn_terbit,
                'pengarang' => $request->pengarang,
            ]);

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil merubah data.'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal merubah data.'
                ], 500);
            }

        }else{
            abort(404);
        }   
    }
    
    public function destroy($uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Buku::findByUuid($uuid);

            if (!$data) {
                return response()->json([
                    'status' => false,
                    'message' => 'data tidak ada'
                ], 400);
            }

            $data->delete();

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menghapus data '
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menghapus data.'
                ], 500);
            }

        }else{
            abort(404);   
        }
    }

}
