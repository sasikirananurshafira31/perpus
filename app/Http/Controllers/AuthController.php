<?php

namespace App\Http\Controllers;

use App\Models\User;
use JWTAuth,AppHelper;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Validator, DB, Hash, Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json(['status'=> false, 'message'=> $validator->messages()->first()], 400);
        }
        
        // $credentials['is_verified'] = 1;
        
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => 'Email/Password salah! <br> atau <br> Email belum dikonfirmasi.'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Sesuatu error terjadi.'], 500);
        }
        return response()->json(['status' => true], 200)->header('Authorization', $token);
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->bearerToken());
            return response()->json(['status' => true, 'message'=> "Berhasil logout."]);
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Sesuatu error terjadi.'], 500);
        }
    }

    public function register(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        // return $request;
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success'], 200);
    }

    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();
        if(!is_null($check)){
            $user = User::find($check->user_id);
            if($user->is_verified == 1){
                return response()->json([
                    'status'=> false,
                    'message'=> 'Alamat Email sudah diverifikasi.'
                ], 400);
            }
            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();
            return response()->json([
                'status'=> true,
                'message'=> 'Alamat email berhasil diverifikasi.'
            ]);
        }
        return response()->json(['status'=> false, 'message'=> "Kode verifikasi tidak valid."], 400);
    }

    public function recover(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            $error_message = "Alamat email Anda tidak ditemukan.";
            return response()->json(['status' => false, 'message' =>  $error_message], 404);
        }

        try {
            $token = AppHelper::generateForgotToken();

            $cek = DB::table('password_resets')->where('email', $request->email)->first();

            if(isset($cek->email)){
                $ubahStatus = [
                    'token' => $token,
                ];
                DB::table('password_resets')->where('email',$request->email)->update($ubahStatus);
            }else{
                DB::table('password_resets')->insert(
                    [
                        'email' => $request->email,
                        'token' => $token,
                        'created_at' => new \DateTime(),
                    ]
                );
            }


            $name = $user->name;
            $email = $user->email;
            $subject = "Lupa Password?";

            Mail::send('emails.forgot', ['name' => $name, 'token' => $token],
                function($mail) use ($email, $name, $subject){
                    $mail->from(getenv('MAIL_USERNAME'), "Dinas Koperasi Usaha Kecil & Menengah");
                    $mail->to($email, $name);
                    $mail->subject($subject);
                });
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['status' => false, 'message' => $error_message], 401);
        }
        return response()->json([
            'status' => true, 'message'=> 'Tautan untuk reset password telah dikirim via email.'
        ]);
    }

    public function checkRecover(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            
            $getToken = DB::table('password_resets')
                        ->where('token',$request->token)
                        ->first();

            if (!is_null($getToken)) {
                return response()->json(['status'=> true, 'message'=> "Token valid."]);
            }

            return response()->json(['status'=> false, 'message'=> "Token tidak valid/expired."], 400);
        }else{
            abort(404);
        }
    }

    public function postRecover(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $rules = [
                'password' => 'required|min:6',
                'repassword' => 'required|min:6',
            ];
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {
                return response()->json(['status'=> false, 'message'=> $validator->messages()->first()], 400);
            }

            $forgetPassword = DB::table('password_resets')
                                ->where('token', $request->input('token'))
                                ->first();

            if (!is_null($forgetPassword)) {
                $user = \App\Models\User::where('email', $forgetPassword->email)->first();
                $user->password=bcrypt($request->password);

                if($user->update()){
                    DB::table('password_resets')->where('token',$request->token)->delete();
                    return response()->json(['status'=> true, 'message'=> "Berhasil membuat password baru."]);
                }            
            }

            return response()->json(['status'=> false, 'message'=> "Token tidak valid/expired."], 400);
        }else{
            abort(404);
        }
    }

    public function user(Request $request)
    {
        $user = User::find(JWTAuth::user()->id);
        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }

    public function refresh()
    {
        if ($token = JWTAuth::refresh(JWTAuth::getToken())) {
            return response()
                ->json(['status' => true], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['status' => false,'message' => 'refresh_token_error'], 401);
    }

    public function testing(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = User::where(function($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('email', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->paginate($per);

            // Add Columns
            $user->map(function($a) {
                $a->action = '<span class="btn btn-sm btn-clean btn-icon btn-icon-md btn-detail" title="Detail" data-id="'.$a->uuid.'"><i class="la la-eye kt-font-info"></i></span>';
                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }
}