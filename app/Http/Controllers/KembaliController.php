<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Siswa;
use Validator;
use App\Models\Aktivitas;
use Illuminate\Http\Request;

class KembaliController extends Controller
{
    public function getsiswa($nisn)
    {
        $siswa = Siswa::where('nisn', '=', $nisn)->first();

        if($siswa){
            return response()->json([
                'data' => $siswa->nama
            ]);
        } else {
            return response()->json([
                'pesan' => 'tidak ada data siswa'
            ], 400);
        }
    }

    public function getbuku($kode)
    {
        $buku = Buku::where('kode', '=', $kode)->first();
        if($buku && $buku->status != 0){
            return response()->json([
                'data' => $buku->judul_buku
            ]);
        } else if($buku->status == 0){
            return response()->json([
                'pesan' => 'buku belum dipinjam'
            ], 400);
        } else {
            return response()->json([
                'pesan' => 'tidak ada data buku'
            ], 400);
        }
    }

    public function store(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $validator = Validator::make($request->all(), [
                'kode_buku'  => 'required|string',
                'nisn'  => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status'    => false,
                    'message'   => $validator->messages()->first()
                ], 400);
            }
            $siswa = Siswa::where('nisn','=', $request->nisn)->first(); 
            $buku = Buku::where('kode','=', $request->kode_buku)->first(); 
            if ($buku->status == '0') {
                return response()->json([
                    'status'    => false,
                    'message'   => 'buku belum dipinjam'
                ], 400);
            }
            $data = Aktivitas::where([['buku_id', '=', $buku->id], ['siswa_id', '=', $siswa->id]])->first();
            if($data == null){
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal Mengembalikan buku.'
                ],400);
            }
            $data->update([
                'tgl_pengembalian' => date('Y-m-d'),
                'status' => '1'
            ]);
            $buku->update(['status' => '0']);

            
            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menambah data.'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menambah data.'
                ], 500);
            }

        }else{
            abort(404);
        }     
    }
}
