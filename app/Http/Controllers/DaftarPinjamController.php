<?php

namespace App\Http\Controllers;

use App\Models\Aktivitas;
use App\Models\Buku;
use Illuminate\Http\Request;

class DaftarPinjamController extends Controller
{
    public function index(Request $request)
    {
        if (request()->wantsJson() && request()->ajax()) {
            // Set Request Per Page
            $per = (($request->per) ? $request->per : 10);
            
            // Get User By Search And Per Page
            $user = Aktivitas::with(['buku', 'siswa'])->whereHas('buku',function($q) use ($request) {
                $q->where('kode', 'LIKE', '%'.$request->search.'%');
            })->orWhereHas('siswa', function($a) use ($request){
                $a->where('nama', 'LIKE', '%'.$request->search.'%');
                $a->orWhere('nisn', 'LIKE', '%'.$request->search.'%');
            })->orderBy('id','asc')->paginate($per);

            // Add Columns
            $user->map(function($a) {
            	if ($a->status == "0") {
            		$a->status = "Tersedia";
            	} else {
            		$a->status = "Telah Dipinjam    ";
            	}
                $a->action = '<button class="btn btn-sm btn-clean delete" title="Hapus" data-id="'.$a->uuid.'"><i class="las la-trash p-warnaa text-danger"></i></button>';

                return $a;
            });
            return response()->json($user);

        }else{
            abort(404);
        }
    }

    public function destroy($uuid)
    {
        if (request()->wantsJson() && request()->ajax()) {
            $data = Aktivitas::findByUuid($uuid, 'buku');

            if (!$data) {
                return response()->json([
                    'status' => false,
                    'message' => 'data tidak ada'
                ], 400);
            }

            if($data->buku->status == 1){
                Buku::findByUuid($data->buku->uuid)->update([
                    'status' => '0'
                ]);
            }

            $data->delete();

            if($data) {
                return response()->json([
                    'status' => true,
                    'data' => 'Berhasil menghapus data '
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => 'Gagal menghapus data.'
                ], 500);
            }

        }else{
            abort(404);   
        }
    }



}
