<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Aktivitas extends Model
{
    use Uuid;
        protected $fillable = [
        'uuid', 'status', 'tgl_pinjam', 'tgl_pengembalian', 'buku_id', 'user_id', 'denda'
    ];

    public function buku()
    {
        return $this->hasOne(Buku::class, 'id', 'buku_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
