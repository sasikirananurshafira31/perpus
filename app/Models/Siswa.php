<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Siswa extends Model
{
    use Uuid;
    protected $fillable = [
        'uuid', 'nama', 'nisn', 'jenis_kelamin', 'kelas', 'alamat'
    ];
}
