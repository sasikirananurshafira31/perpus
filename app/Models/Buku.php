<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
	use Uuid;
        protected $fillable = [
        'uuid', 'tags_id', 'kode', 'status', 'judul_buku', 'foto'
    ];

    protected $appends = ['foto_dir'];

    public function tag()
    {
        return $this->hasOne(Tag::class, 'id', 'tags_id');
    }

    public function detail_buku()
    {
        return $this->hasOne(BukuDetail::class, 'buku_id', 'id');
    }

    function getFotoDirAttribute(){
        return asset('images/foto_buku/'.$this->foto);
    }

}
