<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class UserDetail extends Model
{
    use Uuid;

    protected $fillable = ['uuid', 'jenis_kelamin', 'user_id', 'no_telp'];
}
