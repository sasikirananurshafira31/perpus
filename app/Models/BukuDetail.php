<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class BukuDetail extends Model
{
    use Uuid;
    protected $fillable = [
        'penerbit', 'thn_terbit', 'uuid', 'pengarang', 'buku_id'
    ];

    public function buku()
    {
        return $this->hasOne(Buku::class, 'id', 'buku_id');
    }
}
