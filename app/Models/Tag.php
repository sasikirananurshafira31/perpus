<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Tag extends Model
{
    use Uuid;
    protected $fillable = [
        'uuid', 'nama'
    ];
}
