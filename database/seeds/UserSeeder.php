<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::create(
    		[
    			'name' => 'Administrator',
    			'email' => 'admin@eperpus.com',
    			'password' => bcrypt('12345678'),
    			'level' => 'admin',
    		]
    	);

        User::create(
            [
                'name' => 'user',
                'email' => 'user@eperpus.com',
                'password' => bcrypt('12345678'),
                'level' => 'user',
            ]
        );

    }
}
